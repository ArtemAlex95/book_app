# README

Book review app

To start application:

Copy .env.default as .env

bundle install

rails db:create db:migrate db:seed

Generate docs:

rake rswag:specs:swaggerize
