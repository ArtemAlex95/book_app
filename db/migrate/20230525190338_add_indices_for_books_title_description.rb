# frozen_string_literal: true

class AddIndicesForBooksTitleDescription < ActiveRecord::Migration[7.0]
  def change
    reversible do |dir|
      dir.up do
        execute <<-SQL
          CREATE INDEX index_books_on_title_trigram ON books USING gin (title gin_trgm_ops);
          CREATE INDEX index_books_on_description_trigram ON books USING gin (description gin_trgm_ops);
        SQL
      end

      dir.down do
        execute <<-SQL
          DROP INDEX index_books_on_title_trigram;
          DROP INDEX index_books_on_description_trigram;
        SQL
      end
    end
  end
end
