# frozen_string_literal: true

class CreateBooks < ActiveRecord::Migration[7.0]
  def change
    create_table :books do |t|
      t.references :author, null: false, foreign_key: true
      t.bigint :isbn, null: false
      t.string :title, null: false
      t.text :description
      t.integer :status, null: false
      t.integer :publication_year
      t.integer :reviews_count, default: 0

      t.timestamps
    end

    add_index :books, :isbn, unique: true
  end
end
