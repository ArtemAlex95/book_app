# frozen_string_literal: true

User.create!(
  name: FFaker::Name.name,
  email: FFaker::Internet.email,
  password: 'foobarfoobar123',
  role: :moderator
)

50.times do
  User.create!(
    name: FFaker::Name.name,
    email: FFaker::Internet.email,
    password: FFaker::Internet.password,
    role: :reviewer
  )
end

40.times do
  Author.create!(
    name: FFaker::Name.name,
    bio: FFaker::Lorem.paragraph
  )
end

Book.transaction do
  100.times do
    author = Author.all.sample
    status = %w[draft beta published].sample

    book = Book.create!(
      author:,
      isbn: FFaker::Book.isbn,
      title: FFaker::Book.title,
      description: FFaker::Book.description,
      status:,
      publication_year: status == 'published' ? rand(1900..1999) : nil
    )

    50.times do
      user = User.all.sample

      Review.create!(
        body: FFaker::Lorem.paragraph,
        score: rand(1..5),
        user:,
        book:
      )
    end
  end
end
