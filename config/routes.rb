# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  namespace :api do
    resource :users, only: :create
    resource :sessions, only: :create
    resources :books, only: %i[index show] do
      resources :reviews, only: %i[index create destroy]
    end
  end
end
