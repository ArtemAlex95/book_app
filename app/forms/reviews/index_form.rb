# frozen_string_literal: true

module Reviews
  class IndexForm < PaginateForm
    params do
      required(:book_id).value(:integer, gteq?: 1)
    end
  end
end
