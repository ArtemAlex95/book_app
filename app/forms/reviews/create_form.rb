# frozen_string_literal: true

module Reviews
  class CreateForm < ApplicationForm
    params do
      required(:book_id).value(:integer, gteq?: 1)
      required(:review).hash do
        optional(:body).value(:string)
        required(:rating).value(:integer, included_in?: 1..5)
      end
    end
  end
end
