# frozen_string_literal: true

module Reviews
  class DestroyForm < ApplicationForm
    params do
      required(:book_id).value(:integer, gteq?: 1)
      required(:id).value(:integer, gteq?: 1)
    end
  end
end
