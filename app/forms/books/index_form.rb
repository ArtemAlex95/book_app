# frozen_string_literal: true

module Books
  class IndexForm < PaginateForm
    params do
      optional(:q).value(:string)
    end
  end
end
