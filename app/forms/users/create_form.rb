# frozen_string_literal: true

module Users
  class CreateForm < ApplicationForm
    params do
      required(:name).value(:string, min_size?: 2, max_size?: 20)
      required(:email).value(:string)
      required(:password).value(:string, min_size?: 8, max_size?: 30)
    end

    rule(:email).validate(:email_format)
  end
end
