# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Localable
  include ErrorsHandlerable
  include DryValidatable
  include HeaderAuthorizable
  include JsonRenderable
  include PolicyAuthorizable
end
