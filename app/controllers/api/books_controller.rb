# frozen_string_literal: true

module Api
  class BooksController < ApplicationController
    skip_before_action :validate_user

    def index
      valid_params = validate_with(Books::IndexForm, params)
      result = Books::Index.new.call(valid_params)

      render_blueprint(
        result[:data],
        Books::SingleBlueprint,
        options: {
          root: 'data',
          meta: {
            total: result[:data_count],
            page: valid_params[:page] || 1,
            pages: result[:total_pages]
          }
        }
      )
    end

    def show
      book = Book.find(params[:id])

      render_blueprint(book, Books::SingleBlueprint)
    end
  end
end
