# frozen_string_literal: true

module Api
  class ReviewsController < ApplicationController
    skip_before_action :validate_user, only: :index

    authorize_with! only: :destroy do
      Users::ModeratorPolicy.new.call(current_user)
    end

    def index
      valid_params = validate_with(Reviews::IndexForm, params)
      result = Reviews::Index.new.call(valid_params)

      render_blueprint(
        result[:data],
        Reviews::SingleBlueprint,
        options: {
          root: 'data',
          meta: {
            total: result[:data_count],
            page: valid_params[:page] || 1,
            pages: result[:total_pages]
          }
        }
      )
    end

    def create
      valid_params = validate_with(Reviews::CreateForm, params)
      review = Reviews::Create.new.call(current_user,
                                        book_id: valid_params[:book_id],
                                        body: valid_params[:review][:body],
                                        rating: valid_params[:review][:rating])

      render_blueprint(review, Reviews::SingleBlueprint, status: :created)
    end

    def destroy
      valid_params = validate_with(Reviews::DestroyForm, params)
      Reviews::Destroy.new.call(book_id: valid_params[:book_id], id: valid_params[:id])

      render_ok
    end
  end
end
