# frozen_string_literal: true

module Api
  class UsersController < ApplicationController
    skip_before_action :validate_user

    def create
      valid_params = validate_with(Users::CreateForm, params)
      user = Users::Create.new.call(name: valid_params[:name], email: valid_params[:email],
                                    password: valid_params[:password])
      response.headers['X-USER-ID'] = user.id

      render_blueprint(user, Users::SingleBlueprint, status: :created)
    end
  end
end
