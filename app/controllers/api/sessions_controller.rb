# frozen_string_literal: true

module Api
  class SessionsController < ApplicationController
    skip_before_action :validate_user

    def create
      valid_params = validate_with(Sessions::CreateForm, params)
      user = Sessions::Create.new.call(email: valid_params[:email], password: valid_params[:password])
      response.headers['X-USER-ID'] = user.id

      render_blueprint(user, Users::SingleBlueprint)
    end
  end
end
