# frozen_string_literal: true

module ErrorsHandlerable
  extend ActiveSupport::Concern
  included do
    rescue_from StandardError do |e|
      render json: { errors: e.as_json }, status: 500
    end

    rescue_from Api::BadRequestError do |e|
      render json: { errors: e.error_hash }, status: :bad_request
    end

    rescue_from Api::UnauthorizedError do |e|
      render json: { errors: e.error_hash }, status: :unauthorized
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      render json: {
               errors: {
                 title: I18n.t('api.errors.record_not_found'),
                 status: 404,
                 detail: I18n.t('api.errors.messages.record_not_exists'),
                 model_name: e.model
               }
             },
             status: 404
    end

    rescue_from Api::AccessForbiddenError do |e|
      render json: { errors: e.error_hash }, status: 403
    end
  end
end
