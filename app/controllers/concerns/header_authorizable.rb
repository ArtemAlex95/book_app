# frozen_string_literal: true

module HeaderAuthorizable
  extend ActiveSupport::Concern

  included do
    before_action :validate_user
  end

  def validate_user
    payload = request.headers['X-USER-ID']
    user = User.where(id: payload).first

    raise Api::UnauthorizedError if user.nil?
  end

  def current_user
    @current_user ||= begin
      payload = request.headers['X-USER-ID']
      User.find(payload)
    end
  end
end
