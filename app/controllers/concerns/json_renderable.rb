# frozen_string_literal: true

module JsonRenderable
  extend ActiveSupport::Concern

  def render_ok
    render json: {}, status: :ok
  end

  def render_blueprint(entity, entity_blueprint, status: :ok, options: {})
    render json: entity_blueprint.render(entity, options), status:
  end
end
