# frozen_string_literal: true

module Users
  class ModeratorPolicy
    def call(user)
      user.moderator?
    end
  end
end
