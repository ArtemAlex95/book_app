# frozen_string_literal: true

module Api
  class RecordNotFoundError < StandardError
    attr_reader :status, :detail

    def initialize(message = nil)
      super
      @detail = message
      @status = 404
    end

    def error_hash
      {
        title: I18n.t('api.errors.record_not_found'),
        status: @status,
        detail: @detail || I18n.t('api.errors.messages.record_not_exists')
      }
    end
  end
end
