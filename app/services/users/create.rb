# frozen_string_literal: true

module Users
  class Create
    def call(name:, email:, password:)
      User.create!(name:, email:, password:, role: :reviewer)
    end
  end
end
