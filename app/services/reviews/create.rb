# frozen_string_literal: true

module Reviews
  class Create
    def call(user, book_id:, body:, rating:)
      book = Book.find(book_id)

      if user.reviewed_books.include?(book)
        raise Api::BadRequestError,
              { review: I18n.t('api.errors.messages.already_reviewed') }
      end

      ActiveRecord::Base.transaction do
        review = book.reviews.create!(body:, user:, score: rating)
        book.with_lock do
          book.update!(reviews_count: book.reviews.count)
        end

        review
      end
    end
  end
end
