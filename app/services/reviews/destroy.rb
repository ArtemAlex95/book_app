# frozen_string_literal: true

module Reviews
  class Destroy
    def call(book_id:, id:)
      book = Book.find(book_id)

      ActiveRecord::Base.transaction do
        book.reviews.find(id).destroy!
        book.with_lock do
          book.update!(reviews_count: book.reviews.count)
        end
      end
    end
  end
end
