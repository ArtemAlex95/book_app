# frozen_string_literal: true

module Sessions
  class Create
    def call(email:, password:)
      user = User.find_by(email:)

      return user if user&.authenticate(password)

      raise Api::UnauthorizedError, I18n.t('api.errors.messages.incorrect_creds')
    end
  end
end
