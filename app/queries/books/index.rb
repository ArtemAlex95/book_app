# frozen_string_literal: true

module Books
  class Index < ApplicationQuery
    def call(params)
      books = Book.all
      if params[:q]
        books = books.where('similarity(title, ?) > 0.3 OR similarity(description, ?) > 0.05', "%#{params[:q]}%",
                            "%#{params[:q]}%")
      end
      books = books.page(params[:page]).preload(:author)

      prepare_result(books)
    end
  end
end
