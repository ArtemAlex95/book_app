# frozen_string_literal: true

module Reviews
  class Index < ApplicationQuery
    def call(params)
      book = Book.find(params[:book_id])
      reviews = book.reviews.page(params[:page]).preload(:user)

      prepare_result(reviews)
    end
  end
end
