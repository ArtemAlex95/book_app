# frozen_string_literal: true

class ApplicationQuery
  def prepare_result(results)
    {
      data: results,
      data_count: results.total_count,
      total_pages: results.total_pages
    }
  end
end
