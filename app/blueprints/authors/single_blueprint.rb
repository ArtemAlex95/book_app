# frozen_string_literal: true

module Authors
  class SingleBlueprint < Blueprinter::Base
    identifier :id
    field :name
  end
end
