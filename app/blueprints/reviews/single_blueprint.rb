# frozen_string_literal: true

module Reviews
  class SingleBlueprint < Blueprinter::Base
    identifier :id
    fields :body, :score
    association :user, name: :reviewer, blueprint: Users::SingleBlueprint
  end
end
