# frozen_string_literal: true

module Books
  class SingleBlueprint < Blueprinter::Base
    identifier :id
    fields :isbn, :title, :description, :status, :publication_year, :reviews_count
    association :author, blueprint: Authors::SingleBlueprint
  end
end
