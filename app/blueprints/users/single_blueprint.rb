# frozen_string_literal: true

module Users
  class SingleBlueprint < Blueprinter::Base
    identifier :id
    field :name
  end
end
