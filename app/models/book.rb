# frozen_string_literal: true

class Book < ApplicationRecord
  belongs_to :author
  has_many :reviews, dependent: :destroy

  enum status: %i[draft beta published].freeze
end
