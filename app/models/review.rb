# frozen_string_literal: true

class Review < ApplicationRecord
  belongs_to :user
  belongs_to :book, counter_cache: true
end
