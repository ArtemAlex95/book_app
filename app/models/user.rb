# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password
  has_many :reviews, dependent: :destroy
  has_many :reviewed_books, through: :reviews, source: :book

  enum role: %i[reviewer moderator].freeze
end
