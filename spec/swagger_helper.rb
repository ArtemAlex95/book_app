# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.join('swagger').to_s

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    'v1/swagger.yaml' => {
      openapi: '3.0.1',
      info: {
        title: 'API V1',
        version: 'v1'
      },
      paths: {},
      components: {
        securitySchemes: {
          'X-USER-ID': {
            description: 'Simplification for authentication',
            type: :apiKey,
            name: 'X-USER-ID',
            in: :header
          }
        },
        schemas: {
          humans: Entities::Humans::HUMANS_SCHEMA,
          books: Entities::Books::INDEX_BOOKS_SCHEMA,
          book: Entities::Books::BOOK_SCHEMA,
          reviews: Entities::Reviews::INDEX_REVIEWS_SCHEMA,
          review: Entities::Reviews::REVIEW_SCHEMA,
          error_forbidden: Entities::Errors::ERROR_SCHEMA_FORBIDDEN,
          error_not_found: Entities::Errors::ERROR_SCHEMA_NOT_FOUND,
          error_bad_request: Entities::Errors::ERROR_SCHEMA_BAD_REQUEST,
          error_unauthorized: Entities::Errors::ERROR_SCHEMA_UNAUTHORIZED
        }
      },
      servers: [
        {
          url: ENV['ROUTES_HOST'],
          variables: {
            defaultHost: {
              default: ENV['ROUTES_HOST']
            }
          }
        }
      ]
    }
  }

  # Specify the format of the output Swagger file when running 'rswag:specs:swaggerize'.
  # The swagger_docs configuration option has the filename including format in
  # the key, this may want to be changed to avoid putting yaml in json files.
  # Defaults to json. Accepts ':json' and ':yaml'.
  config.swagger_format = :yaml
end
