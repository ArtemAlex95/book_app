# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reviews::Destroy do
  subject { described_class.new.call(book_id: data[:book_id], id: data[:id]) }

  let(:book) { create(:book) }
  let(:user) { create(:user) }
  let(:review) { create(:review, book:, user:) }
  let(:data) { { book_id: book.id, id: review.id } }

  context 'with correct data' do
    it 'delete review, reviews count decreasing' do
      subject
      expect(book.reviews.count).to eq 0
      expect(book.reload.reviews_count).to eq 0
    end
  end

  context 'with incorrect data' do
    let(:data) { { book_id: 0 } }
    it 'raise error' do
      expect { subject }.to raise_error ActiveRecord::RecordNotFound
    end
  end
end
