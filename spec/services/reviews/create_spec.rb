# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reviews::Create do
  subject do
    described_class.new.call(user, book_id: data[:book_id], body: data[:review][:body], rating: data[:review][:rating])
  end

  let(:user) { create(:user) }
  let(:book) { create(:book) }
  let(:book1) { create(:book) }
  let!(:review) { create(:review, user:, book: book1) }
  let(:body) { 'buddy' }
  let(:rating) { 5 }
  let(:data) { { review: { body:, rating: }, book_id: book.id } }

  context 'with valid params' do
    it 'creates review' do
      result = subject
      review = Review.last
      expect(book.reviews.last).to eq result
      expect(review.body).to eq body
      expect(review.score).to eq rating
      expect(book.reload.reviews_count).to eq 1
    end
  end

  context 'does not create review' do
    context 'without score' do
      let(:data) { { review: { body: }, book_id: book.id } }

      it 'fails' do
        expect { subject }.to raise_error ActiveRecord::NotNullViolation
      end
    end

    context 'already reviewed' do
      let(:data) { { review: { body: }, book_id: book1.id } }

      it 'fails' do
        expect { subject }.to raise_error Api::BadRequestError
      end
    end
  end
end
