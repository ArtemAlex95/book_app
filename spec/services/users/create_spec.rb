# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Users::Create do
  subject { described_class.new.call(name: data[:name], email: data[:email], password: data[:password]) }

  let(:name) { FFaker::Name.name }
  let(:email) { FFaker::Internet.email }
  let(:password) { FFaker::Internet.password }
  let(:data) { { name:, email:, password: } }

  context 'with valid params' do
    it 'creates user' do
      result = subject
      user = User.last
      expect(result).to eq user
      expect(user.email).to eq email
      expect(user.name).to eq name
    end
  end

  context 'does not create user' do
    context 'without email' do
      let(:data) { { name:, password: } }

      it 'fails' do
        expect { subject }.to raise_error ActiveRecord::NotNullViolation
      end
    end

    context 'without password' do
      let(:data) { { name:, email: } }

      it 'fails' do
        expect { subject }.to raise_error ActiveRecord::RecordInvalid
      end
    end

    context 'without name' do
      let(:data) { { email:, password: } }

      it 'fails' do
        expect { subject }.to raise_error ActiveRecord::NotNullViolation
      end
    end
  end
end
