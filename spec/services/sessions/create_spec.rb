# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Sessions::Create do
  subject { described_class.new.call(email: data[:email], password: data[:password]) }

  let(:password) { 'password123' }
  let!(:user) { create(:user, password:) }
  let(:email) { user.email }
  let(:data) { { email:, password: } }

  context 'successful session create' do
    context 'success' do
      it 'session created' do
        result = subject
        expect(result).to eq user
      end
    end
  end

  context 'failure' do
    context 'incorrect email' do
      let(:another_email) { 'incorrect@mail.ru' }
      let(:data) { { email: another_email, password: } }

      it 'fails to login' do
        expect { subject }.to raise_error Api::UnauthorizedError
      end
    end

    context 'incorrect password' do
      let(:another_password) { 'incorrect_password' }
      let(:data) { { email:, password: another_password } }

      it 'fails to login' do
        expect { subject }.to raise_error Api::UnauthorizedError
      end
    end
  end
end
