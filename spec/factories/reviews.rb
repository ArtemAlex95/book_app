# frozen_string_literal: true

FactoryBot.define do
  factory :review do
    user
    book
    body { FFaker::Lorem.paragraph }
    score { rand(1..5) }
  end
end
