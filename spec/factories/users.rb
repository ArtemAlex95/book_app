# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { FFaker::Name.name }
    email { FFaker::Internet.email }
    password { FFaker::Internet.password }
    role { :reviewer }

    trait :moderator do
      role { :moderator }
    end
  end
end
