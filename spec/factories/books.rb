# frozen_string_literal: true

FactoryBot.define do
  factory :book do
    author
    isbn { FFaker::Book.isbn }
    title { FFaker::Book.title }
    description { FFaker::Book.description }
    status { %w[draft beta published].sample }
    publication_year { status == 'published' ? rand(1900..1999) : nil }
  end
end
