# frozen_string_literal: true

module Entities
  module Books
    INDEX_BOOKS_SCHEMA = {
      type: :object,
      properties: {
        data: {
          type: :array,
          items: {
            type: :object,
            properties: {
              id: { type: :integer, example: 1, required: true },
              isbn: { type: :integer, example: 9_780_307_264_787, required: true },
              title: { type: :string, example: 'Example', required: true },
              description: { type: :string, example: 'Moscow' },
              status: { type: :string, example: 'beta', required: true },
              publication_year: { type: :integer, example: 1999, nullable: true },
              reviews_count: { type: :integer, example: 20, required: true },
              author: { '$ref': '#/components/schemas/humans' }
            }
          }
        },
        meta: {
          type: :object,
          properties: {
            total: { type: :integer, example: 1, required: true },
            page: { type: :integer, example: 1, required: true },
            pages: { type: :integer, example: 1, required: true }
          }
        }
      }
    }.freeze
  end
end
