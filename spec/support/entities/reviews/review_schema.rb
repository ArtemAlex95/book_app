# frozen_string_literal: true

module Entities
  module Reviews
    REVIEW_SCHEMA = {
      type: :object,
      properties: {
        id: { type: :integer, example: 1, required: true },
        body: { type: :string, example: 'Example', nullable: true },
        rating: { type: :integer, example: 5, required: true },
        reviewer: { '$ref': '#/components/schemas/humans' }
      }
    }.freeze
  end
end
