# frozen_string_literal: true

module Entities
  module Reviews
    INDEX_REVIEWS_SCHEMA = {
      type: :object,
      properties: {
        data: {
          type: :array,
          items: {
            type: :object,
            properties: {
              id: { type: :integer, example: 1, required: true },
              body: { type: :string, example: 'Example', nullable: true },
              rating: { type: :integer, example: 5, required: true },
              reviewer: { '$ref': '#/components/schemas/humans' }
            }
          }
        },
        meta: {
          type: :object,
          properties: {
            total: { type: :integer, example: 1, required: true },
            page: { type: :integer, example: 1, required: true },
            pages: { type: :integer, example: 1, required: true }
          }
        }
      }
    }.freeze
  end
end
