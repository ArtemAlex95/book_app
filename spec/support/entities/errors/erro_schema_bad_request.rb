# frozen_string_literal: true

module Entities
  module Errors
    ERROR_SCHEMA_BAD_REQUEST = {
      type: :object,
      properties: {
        errors: {
          type: :object,
          properties: {
            title: { type: :string, nullable: true, example: 'string' },
            status: { type: :integer, example: '400' },
            detail: { type: :string, nullable: true, example: 'string' },
            source: {
              type: :object,
              properties: {
                pointer: { type: :string, nullable: true, example: 'data/attributes/<field>' }
              }
            }
          }
        }
      }
    }.freeze
  end
end
