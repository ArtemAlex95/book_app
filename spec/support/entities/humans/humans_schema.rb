# frozen_string_literal: true

module Entities
  module Humans
    HUMANS_SCHEMA = {
      type: :object,
      properties: {
        id: { type: :integer, example: 1, required: true },
        name: { type: :string, required: true }
      }
    }.freeze
  end
end
