# frozen_string_literal: true

RSpec.shared_examples 'with error response 401' do |message|
  schema '$ref': '#/components/schemas/error_unauthorized'
  let(:message) { message }

  run_test! do |response|
    expect(response.status).to eq 401
    expect(response_errors['detail']).to eq(message).or eq I18n.t('api.errors.messages.not_logged_in')
  end
end

RSpec.shared_examples 'with error response 403' do |message|
  schema '$ref': '#/components/schemas/error_forbidden'
  let(:message) { message }

  run_test! do |response|
    expect(response.status).to eq 403
    expect(response_errors['detail']).to eq(message).or eq I18n.t('api.errors.messages.access_forbidden')
  end
end

RSpec.shared_examples 'with error response 404' do |message|
  schema '$ref': '#/components/schemas/error_not_found'
  let(:message) { message }

  run_test! do |response|
    expect(response.status).to eq 404
    expect(response_errors['detail']).to eq(message).or eq I18n.t('api.errors.messages.record_not_exists')
  end
end

RSpec.shared_examples 'with error response 400' do |message|
  schema '$ref': '#/components/schemas/error_bad_request'
  let(:message) { message }

  run_test! do |response|
    expect(response.status).to eq 400
    expect(response_errors['detail']).to eq message
  end
end
