# frozen_string_literal: true

RSpec.shared_context :with_moderator do
  security ['X-USER-ID': []]

  let(:user) { create(:user, :moderator) }
  let!(:'X-USER-ID') { user.id }
end
