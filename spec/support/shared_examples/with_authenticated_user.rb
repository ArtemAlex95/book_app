# frozen_string_literal: true

RSpec.shared_context :with_authenticated_user do
  security ['X-USER-ID': []]

  let(:user) { create(:user) }
  let!('X-USER-ID') { user.id }
end
