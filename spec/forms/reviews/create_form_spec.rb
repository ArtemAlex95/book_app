# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reviews::CreateForm, type: :form do
  subject(:form) { described_class.new.call(params) }

  context 'with valid params' do
    let(:params) do
      {
        book_id: 1,
        review: {
          body: 'Buddy',
          rating: 5
        }
      }
    end

    it 'is valid' do
      expect(form).to be_success
      expect(form.errors).to be_empty
    end
  end

  context 'with missing book_id' do
    let(:params) do
      {
        review: {
          body: 'Buddy',
          rating: 5
        }
      }
    end

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:book_id]).to include(I18n.t('dry_validation.errors.key?'))
    end
  end

  context 'with missing review body' do
    let(:params) do
      {
        book_id: 1,
        review: {
          rating: 5
        }
      }
    end

    it 'is valid' do
      expect(form).to be_success
      expect(form.errors).to be_empty
    end
  end

  context 'with missing review rating' do
    let(:params) do
      {
        book_id: 1,
        review: {
          body: 'Buddy'
        }
      }
    end

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:review][:rating]).to include(I18n.t('dry_validation.errors.key?'))
    end
  end

  context 'with invalid book_id' do
    let(:params) do
      {
        book_id: 0,
        review: {
          body: 'Buddy',
          rating: 5
        }
      }
    end

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:book_id]).to include(I18n.t('dry_validation.errors.gteq?', num: 1))
    end
  end

  context 'with invalid review rating' do
    let(:params) do
      {
        book_id: 1,
        review: {
          body: 'Buddy',
          rating: 6
        }
      }
    end

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:review][:rating]).to include(I18n.t('dry_validation.errors.included_in?', list: [1, 2, 3, 4, 5])[:failure])
    end
  end
end
