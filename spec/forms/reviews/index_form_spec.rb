# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reviews::IndexForm, type: :form do
  subject(:form) { described_class.new.call(params) }

  context 'with valid params' do
    let(:params) { { book_id: 1, page: 1 } }

    it 'is valid' do
      expect(form).to be_success
      expect(form.errors).to be_empty
    end
  end

  context 'without book_id' do
    let(:params) { { page: 1 } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:book_id]).to include(I18n.t('dry_validation.errors.key?'))
    end
  end
end
