# # frozen_string_literal: true

# require 'rails_helper'

# RSpec.describe Reviews::DestroyForm do
#   let!(:params) { { book_id: 1, id: 3 } }

#   it_behaves_like :valid_form

#   it_behaves_like :invalid_form, without: :book_id
#   it_behaves_like :invalid_form, without: :id
# end

require 'rails_helper'

RSpec.describe Reviews::DestroyForm, type: :form do
  subject(:form) { described_class.new.call(params) }

  context 'with valid params' do
    let(:params) { { book_id: 1, id: 1 } }

    it 'is valid' do
      expect(form).to be_success
      expect(form.errors).to be_empty
    end
  end

  context 'with missing book_id' do
    let(:params) { { id: 1 } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:book_id]).to include(I18n.t('dry_validation.errors.key?'))
    end
  end

  context 'with missing id' do
    let(:params) { { book_id: 1 } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:id]).to include(I18n.t('dry_validation.errors.key?'))
    end
  end

  context 'with invalid book_id' do
    let(:params) { { book_id: 0, id: 1 } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:book_id]).to include(I18n.t('dry_validation.errors.gteq?', num: 1))
    end
  end

  context 'with invalid id' do
    let(:params) { { book_id: 1, id: 'sobaka' } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:id]).to include(I18n.t('dry_validation.errors.int?'))
    end
  end
end
