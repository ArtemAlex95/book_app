# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PaginateForm, type: :form do
  subject(:form) { described_class.new.call(params) }

  context 'with valid params' do
    let(:params) { { page: 1 } }

    it 'is valid' do
      expect(form.errors).to be_empty
    end
  end

  context 'without params' do
    let(:params) {}

    it 'is valid' do
      expect(form.errors).to be_empty
    end
  end

  context 'invalid page' do
    let(:params) { { page: 0 } }

    it 'is invalid' do
      expect(form.errors[:page]).to include(I18n.t('dry_validation.errors.gteq?', num: 1))
    end
  end

  context 'invalid page' do
    let(:params) { { page: 'sobaka' } }

    it 'is invalid' do
      expect(form.errors[:page]).to include(I18n.t('dry_validation.errors.int?'))
    end
  end
end
