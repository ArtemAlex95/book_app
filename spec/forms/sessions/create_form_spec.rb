# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Sessions::CreateForm, type: :form do
  subject(:form) { described_class.new.call(params) }

  context 'with valid params' do
    let(:params) { { email: 'test@example.com', password: 'password123' } }

    it 'is valid' do
      expect(form).to be_success
      expect(form.errors).to be_empty
    end
  end

  context 'with missing email' do
    let(:params) { { password: 'password123' } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:email]).to include(I18n.t('dry_validation.errors.key?'))
    end
  end

  context 'with invalid email format' do
    let(:params) { { email: 'invalid_email', password: 'password123' } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:email]).to include(I18n.t('api.errors.messages.invalid_email_format'))
    end
  end

  context 'with password length less than 8 characters' do
    let(:params) { { email: 'test@example.com', password: 'short' } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:password]).to include(I18n.t('dry_validation.errors.min_size?', num: 8))
    end
  end

  context 'with password length greater than 30 characters' do
    let(:params) { { email: 'test@example.com', password: 'long_password' * 4 } }

    it 'is invalid' do
      expect(form).to be_failure
      expect(form.errors[:password]).to include(I18n.t('dry_validation.errors.max_size?', num: 30))
    end
  end
end

