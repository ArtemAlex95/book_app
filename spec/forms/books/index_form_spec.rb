# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Books::IndexForm, type: :form do
  subject(:form) { described_class.new.call(params) }

  context 'with valid params' do
    let(:params) { { q: 'test', page: 1 } }

    it 'is valid' do
      expect(form).to be_success
      expect(form.errors).to be_empty
    end
  end

  context 'without params' do
    let(:params) {}

    it 'is valid' do
      expect(form).to be_success
      expect(form.errors).to be_empty
    end
  end
end
