# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Author, type: :model do
  describe 'Associations' do
    it { is_expected.to have_many(:books) }
  end
end
