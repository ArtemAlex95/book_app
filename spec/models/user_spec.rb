# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Associations' do
    it { is_expected.to have_many(:reviews) }
    it { is_expected.to have_many(:reviewed_books) }
  end
end
