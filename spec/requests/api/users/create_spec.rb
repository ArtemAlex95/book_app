# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/users', type: :request do
  path '/api/users' do
    post 'Create user' do
      tags 'Profile'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :data, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string, exmaple: 'Name' },
          email: { type: :string, example: 'mail@example.com' },
          password: { type: :string, exmaple: 'super_password123' }
        },
        required: %i[name email password]
      }

      let(:name) { FFaker::Name.name }
      let(:email) { FFaker::Internet.email }
      let(:password) { FFaker::Internet.password }
      let(:data) { { name:, email:, password: } }

      response '201', 'user registration completed' do
        schema '$ref': '#/components/schemas/humans'

        run_test! do
          expect(response_body['name']).to eq name
        end
      end

      response '400', 'does not create user' do
        let(:data) { { name:, email: } }

        include_examples 'with error response 400', I18n.t('dry_validation.errors.key?')
      end
    end
  end
end
