# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/books', type: :request do
  path '/api/books' do
    get 'Get books' do
      tags 'Books'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :q, in: :query, type: :string, required: false
      parameter name: :page, in: :query, type: :integer, required: false

      let!(:books) { FactoryBot.create_list(:book, 5) }

      response '200', 'get books' do
        schema '$ref': '#/components/schemas/books'

        run_test! do |response|
          expect(response.status).to eq 200
          expect(response_body['data'].pluck('id')).to match_array books.pluck(:id)
        end
      end

      response '400', 'bad params' do
        let(:page) { 0 }

        include_examples 'with error response 400', I18n.t('dry_validation.errors.gteq?', num: 1)
      end
    end
  end
end
