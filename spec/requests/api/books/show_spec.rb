# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/books/{id}', type: :request do
  path '/api/books/{id}' do
    get 'Get book' do
      tags 'Books'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :id, in: :path, type: :integer, required: true

      5.times do |time|
        let(:"book#{time}") { create(:book) }
      end
      let(:id) { book1.id }

      response '200', 'get books' do
        schema '$ref': '#/components/schemas/book'

        run_test! do |response|
          expect(response.status).to eq 200
          expect(response_body['title']).to eq book1.title
        end
      end

      response '404', 'bad params' do
        let(:id) { 0 }

        include_examples 'with error response 404'
      end
    end
  end
end
