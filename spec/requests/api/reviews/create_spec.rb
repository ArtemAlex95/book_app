# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/books/{book_id}/reviews', type: :request do
  path '/api/books/{book_id}/reviews' do
    post 'Create review' do
      tags 'Reviews'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :book_id, in: :path, type: :integer, required: true
      parameter name: :data, in: :body, schema: {
        type: :object,
        properties: {
          review: {
            type: :object,
            properties: {
              body: { type: :string, example: 'buddy', required: false },
              rating: { type: :integer, example: 1, required: true }
            }
          }
        }
      }

      include_context :with_authenticated_user

      let(:book) { create(:book) }
      let(:book_id) { book.id }
      let(:data) { { review: { body: 'buddy', rating: 5 } } }

      response '201', 'create review' do
        schema '$ref': '#/components/schemas/humans'

        run_test! do |response|
          expect(response.status).to eq 201
          expect(Review.last.book).to eq book
        end
      end

      response '401', 'unauthorized error' do
        let('X-USER-ID') { nil }
        let(:book_id) { book.id }
        let(:data) { { review: { body: 'buddy', rating: 5 } } }

        include_examples 'with error response 401'
      end

      response '400', 'bad params' do
        let(:book_id) { book.id }
        let(:data) { { review: { body: 'buddy' } } }
        include_examples 'with error response 400', I18n.t('dry_validation.errors.key?')
      end

      response '404', 'book not found' do
        let(:book_id) { book.id + 1 }
        let(:data) { { review: { body: 'buddy', rating: 5 } } }

        include_examples 'with error response 404'
      end
    end
  end
end
