# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/books/{book_id}/reviews/{id}', type: :request do
  path '/api/books/{book_id}/reviews/{id}' do
    delete 'Destroy review' do
      tags 'Reviews'

      consumes 'application/json'
      produces 'application/json'

      include_context :with_moderator

      parameter name: :book_id, in: :path, type: :integer, required: true
      parameter name: :id, in: :path, type: :integer, required: true

      let(:book) { create(:book) }
      let(:review) { create(:review, book:) }
      let!(:review1) { create(:review, book:) }
      let(:book_id) { book.id }
      let(:id) { review.id }

      response '200', 'destroy review' do
        run_test! do |response|
          expect(response.status).to eq 200
          expect(book.reviews.count).to eq 1
          expect(response_body).to be_empty
        end
      end

      response '404', 'review not found' do
        let(:id) { review1.id + 1 }

        include_examples 'with error response 404'
      end

      response '403', 'access_forbidden' do
        let!(:user) { create(:user) }

        include_examples 'with error response 403'
      end
    end
  end
end
