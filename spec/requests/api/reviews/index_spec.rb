# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/books/{book_id}/reviews', type: :request do
  path '/api/books/{book_id}/reviews' do
    get 'Get reviews' do
      tags 'Reviews'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :book_id, in: :path, type: :integer, required: true
      parameter name: :page, in: :query, type: :integer, required: false

      let(:book) { create(:book) }
      let!(:review) { create(:review, book:) }
      let!(:review1) { create(:review, book:) }
      let(:book_id) { book.id }

      response '200', 'get reviews' do
        schema '$ref': '#/components/schemas/reviews'

        run_test! do |response|
          expect(response.status).to eq 200
          expect(response_body['data'].pluck('id')).to match_array [review.id, review1.id]
        end
      end

      response '400', 'bad params' do
        let(:page) { 0 }

        include_examples 'with error response 400', I18n.t('dry_validation.errors.gteq?', num: 1)
      end

      response '404', 'book not found' do
        let(:book_id) { book.id + 1 }

        include_examples 'with error response 404'
      end
    end
  end
end
