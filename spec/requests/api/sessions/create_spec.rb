# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'api/sessions', type: :request do
  path '/api/sessions' do
    post 'Authorization' do
      tags 'Registration/Authorization'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :data, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string, example: 'exmaple@mail.ru' },
          password: { type: :string, example: 'supersecret' }
        },
        required: %i[email password]
      }

      let!(:user) { create(:user, email:, password:) }
      let(:email) { 'example@mail.ru' }
      let(:password) { 'supersecret' }
      let(:data) { { email:, password: } }

      response '200', 'success authorization' do
        schema '$ref': '#/components/schemas/humans'
        run_test! do |response|
          expect(response_body['id']).to eq user.id
          expect(response.headers['X-USER-ID']).not_to be_nil
        end
      end

      response '401', 'email incorrect' do
        let(:another_email) { 'pasta-macaroni@mail.ru' }
        let(:data) { { email: another_email, password: } }

        include_examples 'with error response 401', I18n.t('api.errors.messages.incorrect_creds')
      end

      response '401', 'password incorrect' do
        let(:another_password) { 'oh_no_my_pass_is_incorrect' }
        let(:data) { { email:, password: another_password } }

        include_examples 'with error response 401', I18n.t('api.errors.messages.incorrect_creds')
      end
    end
  end
end
