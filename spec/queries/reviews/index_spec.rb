# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reviews::Index do
  subject { described_class.new.call(params) }

  let(:book) { create(:book) }
  let!(:reviews) { FactoryBot.create_list(:review, 5, book:) }

  context 'without page' do
    let(:params) { { book_id: book.id } }

    it 'should get all reviews' do
      expect(subject[:data]).to match_array reviews
      expect(subject[:data_count]).to eq Review.count
    end
  end

  context 'with page' do
    let(:params) { { book_id: book.id, page: 2 } }

    it 'should return empty data' do
      expect(subject[:data]).to eq []
    end
  end
end
