# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Books::Index do
  subject { described_class.new.call(params) }

  let!(:book) { create(:book, title: 'Тестовый тайтл') }
  let!(:books) { FactoryBot.create_list(:book, 4) }

  context 'without filters' do
    let(:params) { {} }

    it 'should get all books' do
      expect(subject[:data]).to match_array books << book
      expect(subject[:data_count]).to eq Book.count
    end
  end

  context 'with filters' do
    context 'search without typos' do
      let(:params) { { q: 'тестовый' } }

      it 'should return book with similar title' do
        expect(subject[:data]).to match_array [book]
      end
    end

    context 'search with typos' do
      let(:params) { { q: 'теКстовый' } }

      it 'should return book with similar title' do
        expect(subject[:data]).to match_array [book]
      end
    end

    context 'page' do
      let(:params) { { page: 2 } }

      it 'should return empty data' do
        expect(subject[:data]).to eq []
      end
    end
  end
end
